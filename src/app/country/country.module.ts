import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryDetailsComponent } from './pages/country-details/country-details.component';
import { ByCapitalCityComponent } from './pages/by-capital-city/by-capital-city.component';
import { ByCountryComponent } from './pages/by-country/by-country.component';
import { ByRegionComponent } from './pages/by-region/by-region.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CountryTableComponent } from './components/country-table/country-table.component';
import { CountryInputComponent } from './components/country-input/country-input.component';



@NgModule({
  declarations: [
    CountryDetailsComponent,
    ByCapitalCityComponent,
    ByCountryComponent,
    ByRegionComponent,
    CountryTableComponent,
    CountryInputComponent
  ],
  exports: [
    CountryDetailsComponent,
    ByCapitalCityComponent,
    ByCountryComponent,
    ByRegionComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ]
})
export class CountryModule { }
