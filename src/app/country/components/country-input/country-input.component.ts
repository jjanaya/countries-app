import { Component, EventEmitter, Output, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from "rxjs/operators";

@Component({
  selector: 'app-country-input',
  templateUrl: './country-input.component.html',
  styleUrls: ['./country-input.component.scss']
})
export class CountryInputComponent  implements OnInit  {
  @Output() onEnter     : EventEmitter<string>= new EventEmitter();
  @Output() onDebounce  : EventEmitter<string>= new EventEmitter();
  @Input()  placeholder : string='Country'; 

  debouncer: Subject<string>= new Subject();
  
  term : string='';

  ngOnInit() {
    this.debouncer
    .pipe(debounceTime(350))
    .subscribe(value => {
      console.log('debouncer:', value);
      this.onDebounce.emit(this.term)
    })
  }

  search(){
    this.onEnter.emit(this.term)
    console.log('emitiendo...', this.term);
  }
  
  keyPress(){
    this.debouncer.next( this.term );
  }
}
