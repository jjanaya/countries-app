import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { Country } from '../../interfaces/country.interface';
import { CountryService } from '../../services/country.service';
import { debounceTime } from "rxjs/operators";

@Component({
  selector: 'app-by-region',
  templateUrl: './by-region.component.html',
  styleUrls: ['./by-region.component.scss']
})
export class ByRegionComponent {

  regions      : string[]   = ['africa', 'americas', 'asia', 'europe', 'oceania'];
  activeRegion : string     = '';
  countries    : Country[]  = [];

  constructor( private countryService:CountryService) { }

  getCSSClass (region:string):string {
    return ( region === this.activeRegion ) ? 'btn btn-primary mx-1 ' : 'btn btn-outline-primary mx-1 '
    }

  activateRegion(region:string){
    if (region === this.activeRegion) { return; }
    
    this.activeRegion= region;
    this.countries=[];
    
    this.countryService.getCountriesByRegion(region)
    .subscribe( countries => {
      this.countries = countries;
    })
  }

}
