import { Component } from '@angular/core';
import { Country } from '../../interfaces/country.interface';
import { CountryService } from '../../services/country.service';

@Component({
  selector: 'app-by-country',
  templateUrl: './by-country.component.html',
  styleUrls: ['./by-country.component.scss']
})
export class ByCountryComponent {

  term      : string= '';
  onError   : boolean = false;
  countries : Country[] = [];

  showSugerences      : boolean   = false;
  countriesSugerences : Country[] = []

  constructor(private countryService: CountryService) { }

  search(term:string){
    this.showSugerences = false;
    this.onError=false;
    this.term=term;
    console.log('out:',this.term);
    this.countryService.getCountry(this.term).subscribe ( countries => {
      console.log(countries);
      this.countries=countries;
      

    }, (err) => {
      this.onError=true;
      console.log('Error');
      console.info(err);
    });
  }

sugerence(term:string){
  this.term=term;
  this.showSugerences=true;
  this.onError= false;

  this.countryService.getCountry(term)
    .subscribe(countries=> this.countriesSugerences = countries.splice (0,5)
    , (error) => this.countriesSugerences = []);
  
}

}
