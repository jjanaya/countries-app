import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CountryService } from '../../services/country.service';
import { switchMap, tap } from "rxjs/operators";
import { Country } from '../../interfaces/country.interface';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.scss']
})
export class CountryDetailsComponent implements OnInit {

  country!:Country;

  constructor(
              private activatedRoute:ActivatedRoute, 
              private countryService:CountryService
              ) { }

  ngOnInit(): void {

    this.activatedRoute.params
        .pipe (
          switchMap( (params) => this.countryService.getCountryDetailsByAlphaCode(params.id)),
          tap(console.log)
        ) 
        .subscribe( country => this.country = country);

    /*
    this.activatedRoute.params
      .subscribe( ({id}) =>{
        console.log(id);
        this.countryService.getCountryDetailsByAlphaCode(id)
          .subscribe( country => {
          console.log(country);
          
        })
      });
  */
    }
}
