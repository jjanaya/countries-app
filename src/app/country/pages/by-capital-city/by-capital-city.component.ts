import { Component } from '@angular/core';
import { Country } from '../../interfaces/country.interface';
import { CountryService } from '../../services/country.service';

@Component({
  selector: 'app-by-capital-city',
  templateUrl: './by-capital-city.component.html',
  styleUrls: ['./by-capital-city.component.scss']
})
export class ByCapitalCityComponent  {

  term      : string= 'Test';
  onError   : boolean = false;
  countries : Country[]=[];

  constructor(private countryService: CountryService) { }

  search(term:string){
    this.onError=false;
    this.term=term;
    console.log('out:',this.term);
    this.countryService.getCountryByCapitalCity(this.term).subscribe ( countries => {
      console.log(countries);
      this.countries=countries;
      

    }, (err) => {
      this.onError=true;
      console.log('Error');
      console.info(err);
    });
  }

}
